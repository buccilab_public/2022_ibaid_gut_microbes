rm(list=ls())
setwd("~/Dropbox/IBAID_project/src/stats_ml")
source("./load_packages_and_utils.R") # run script to load packages and utils
source("./load_data.R") # run this if need to load the data

phy <- readRDS("../../data/20190717_IBDAID_phyloseq.RDS")

patient_counts <- table(sample_data(phy)$patient)

# classify: phase use RF classification
# generate the datasets 
class_vars <- c("phase") # set dependent variables
#pred_vars <- c("avgprobiotics","avgadverse","avgvegetable","avgfruitscore","avgnutscore",
#               "avgleanscore","avgfiberscore","avgproscore","avgnoncaloric",
#               "avgbeverscore","avgcondiment_score","avgalcohol_score",
#               "wt","BMI") # set clinical covariate predictors

pred_vars <- c("avgprobiotics","avgfiberscore",
               "wt","BMI")

# select n random datasets with one sample per individual
y <- class_vars[1]
phy_sub <- subset_samples(phy,!is.na(get_variable(phy, y)))

# Remove taxa that have zero contribution to each sample
phy_sub <-  prune_taxa(taxa_sums(phy_sub)> 0, phy_sub)
phy_sub <-  prune_samples(sample_sums(phy_sub)> 0, phy_sub)
phy_sub

#subset only CD patients
phy_sub <-subset_samples(phy_sub,diagnosis == "CD")
  
# Make a dataframe including the Xs and Ys
tdata<- data.frame(t(otu_table(phy_sub)))
tdata<-cbind(class = get_variable(phy_sub,y),
             ID = sample_data(phy_sub)$patient,
             sample_data(phy_sub)[,pred_vars],
             tdata)

tdata$class<-as.character(tdata$class)
tdata$class[tdata$class == "BSL"] <- "0"
tdata$class[tdata$class == "FLUP"] <- "1"
tdata$class[tdata$class == "INT"] <- "2"
tdata$class<- as.factor(tdata$class)

zero_dt <- tdata[tdata$class == "0",]
one_dt <- tdata[tdata$class == "1",]
two_dt <- tdata[tdata$class == "2",]

zero_dt <- zero_dt[!zero_dt$ID  %in% c(one_dt$ID,two_dt$ID),]
one_dt <- one_dt[!one_dt$ID  %in% c(zero_dt$ID,two_dt$ID),]
two_dt <- two_dt[!two_dt$ID  %in% c(zero_dt$ID,one_dt$ID),]

# Only use the sampleID and PatientID
char_dt_zero <-  data.frame(ID = zero_dt$ID, snames = rownames(zero_dt))
char_dt_one <-  data.frame(ID = one_dt$ID, snames = rownames(one_dt))
char_dt_two <-  data.frame(ID = two_dt$ID, snames = rownames(two_dt))

# Create sampled data where we pick samples from each patient once
nSamples <-  100
sam_list <- lapply(1:nSamples,function (x){ 
  set.seed(300+x)
  s_zero <- char_dt_zero  %>% group_by(ID) %>% sample_n(1) %>% as.data.frame()
  set.seed(300+x)
  s_one <- char_dt_one  %>% group_by(ID) %>% sample_n(1) %>% as.data.frame()
  set.seed(300+x)
  s_two <- char_dt_two  %>% group_by(ID) %>% sample_n(1) %>% as.data.frame()
  rbind(s_zero,s_one,s_two)  })
sam_dt <- bind_rows(sam_list, .id = 'source')

set.seed(1)

ns <- 1
ml_input_list <- list()
for(ns in 1:(nSamples/2-1)){
  sel_samples_train <-  sam_dt$snames[which(sam_dt$source == ns)]
  tdata_sub_train <-  tdata[rownames(tdata) %in% sel_samples_train,]
  sel_samples_test <-  sam_dt$snames[which(sam_dt$source == nSamples/2 + ns)]
  tdata_sub_test <-  tdata[rownames(tdata) %in% sel_samples_test,]
  
  # Lets create a test and train dataset for each sampled dataset
  table(tdata_sub_train$class)
  table(tdata_sub_test$class)
  tdata_sub_train$Set <- "train"
  tdata_sub_test$Set <- "test"
  tdata_sub<-rbind(tdata_sub_train,tdata_sub_test)
  ml_input_list[[ns]]<-tdata_sub
}

# source the ml_data class
source('~/Dropbox/ML_methods_project/script/ml_data.R')
mlo_list<-list()
i <- 1
for (i in seq(1,length(ml_input_list))){
  df <- ml_input_list[[i]]
  mlo_k <- ml_data$new()
  mlo_k$load_fulldata_list(df)
  mlo_k$fulldata
  mlo_k$divide_train_and_test()
  mlo_k$run_rf_classification(myseed = 300, ntrees = 3000, ncores = 2, opt_encoding = FALSE)
  mlo_list[[i]]<-mlo_k
}

df_roc<-c()
i<-1
j<-1
for (i in seq(1,length(mlo_list))){
  instance <- mlo_list[[i]]
  if (is.null(instance$rfc_roc)==F){
    temp<-instance$rfc_roc
    temp$iter<-i
    for (j in seq(1,length(temp$rocs))){
      g<-ggroc(temp$roc[[j]])
      gdata <- g$data
      gdata$iter <- i
      gdata$class <- j
      df_roc<-rbind(df_roc,gdata)
    }
  }
}

df_roc$iter<-as.factor(df_roc$iter)
df_roc$class<-as.factor(df_roc$class)

g_roc <- ggplot()+
  geom_line(data=df_roc,aes(x=1-specificity,y=sensitivity,color=class,
                            group=iter),alpha=0.5,size=0.5)+
  #stat_smooth(data=df_roc,aes(x=1-specificity,y=sensitivity,color=class))+
  facet_wrap(~class)+
  xlab("1-Specificity")+
  ylab("Sensitivity")+
  scale_colour_hue(h = c(270, 360))+
  theme_classic()
str_tmp <- "roc_phase_CD"
pdf(paste("./",paste(Sys.Date(),"-plot-",str_tmp,".pdf",sep=''),sep="/"),height = 5, width = 12)
print(g_roc)
dev.off()

df_imp<-c()
i<-1
for (i in seq(1,length(mlo_list))){
  instance <- mlo_list[[i]]
  if (is.null(instance$rfc_importance)==F){
    temp<-instance$rfc_importance
    temp$iter<-i  
    df_imp<-rbind(df_imp,temp)
  }
}

df_imp_filtered<-df_imp[df_imp$p.value<0.05,]
df_imp_filtered<-df_imp_filtered[df_imp_filtered$VarImp>0,]  
df_imp_filtered_summary<-ddply(df_imp_filtered,~Predictors, summarise,
                               mean_imp = mean(VarImp),
                               sd_imp = sd(VarImp))
df_imp_filtered_summary$sd_imp[which(is.na(df_imp_filtered_summary$sd_imp))]<-0

order(df_imp_filtered_summary$mean_imp)

df_imp_filtered_summary$Predictors <- factor(df_imp_filtered_summary$Predictors,
                                             levels = df_imp_filtered_summary$Predictors[order(df_imp_filtered_summary$mean_imp)])
g_var_imp <- ggplot() +
  geom_point(data=df_imp_filtered_summary,
             aes(x=Predictors,y=mean_imp), alpha = 0.8, color='darkblue')+
  geom_errorbar(data=df_imp_filtered_summary,
                aes(x=Predictors,ymax=mean_imp+sd_imp, ymin=mean_imp-sd_imp),color='darkblue')+
  theme_classic()+
  xlab("Predictor")+
  ylab("Mean Permutated Importance")+
  coord_flip()+
  theme(legend.position="none")
g_var_imp
str_tmp <- "var_imp_phase_CD"
pdf(paste("./",paste(Sys.Date(),"-plot-",str_tmp,".pdf",sep=''),sep="/"),height = 6, width = 6)
print(g_var_imp)
dev.off()

ntk<-rev(levels(df_imp_filtered_summary$Predictors))[1:20]

tdata_ntk<-tdata[,as.character(ntk)]
tdata_ntk<-cbind(tdata$class,tdata_ntk)
names(tdata_ntk)[1]<-"class"
tdata_ntk_m<-melt(tdata_ntk,id.vars="class")
tdata_ntk_m$value<-as.numeric(tdata_ntk_m$value)

g_box_imp<-ggplot()+
  geom_boxplot(data=tdata_ntk_m,aes(x=class,y=value,color=class),
               outlier.shape = NA)+
  geom_jitter(data=tdata_ntk_m,aes(x=class,y=value,color=class,
                                   fill=class),shape=21,
              alpha=0.5)+
  theme_classic()+
  scale_fill_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
  scale_color_manual(values = c("#00AFBB", "#E7B800", "#FC4E07"))+
  scale_x_discrete(labels=c("0" = "BSL", "1" = "FLUP", "2" = "INT"))+
  facet_wrap(~variable,scales = "free_y")+
  theme(legend.title = element_blank())
g_box_imp
str_tmp <- "boxplot_imp_phase_CD"
pdf(paste("./",paste(Sys.Date(),"-plot-",str_tmp,".pdf",sep=''),sep="/"),height = 5, width = 12)
print(g_box_imp)
dev.off()
